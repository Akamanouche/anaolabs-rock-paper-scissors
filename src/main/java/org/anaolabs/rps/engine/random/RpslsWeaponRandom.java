/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.random;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * An weapon randomizer for RPSLS game type
 *
 * @author Sylvain RICHET
 *
 */
public class RpslsWeaponRandom implements WeaponRandom {

	/*
	 * (non-Javadoc)
	 * @see org.anaolabs.rps.engine.random.WeaponRandom#nextWeapon()
	 */
	@Override
	public Weapon nextWeapon() {
		Weapon weapon = null;
		Double play = Math.random();

		if (play < 0.20) {
			weapon = Weapon.ROCK;
		} else if (play < 0.40) {
			weapon = Weapon.PAPER;
		} else if (play < 0.60) {
			weapon = Weapon.SCISSORS;
		} else if (play < 0.80) {
			weapon = Weapon.LIZARD;
		} else {
			weapon = Weapon.SPOCK;
		}

		return weapon;
	}

}
