/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.players;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * A human player choosing his weapon
 *
 * @author Sylvain RICHET
 *
 */
public class HumanPlayer implements Player {

	/* Weapon chosen by player */ 
	Weapon weapon = null;
	
	/* (non-Javadoc)
	 * @see org.anaolabs.rps.engine.players.Player#getWeapon()
	 */
	@Override
	public Weapon getWeapon() {
		return weapon;
	}

	/**
	 * Sets the chosen weapon the human player has been previously invited to provide
	 *
	 * @param weapon
	 */
	public void setWeapon(Weapon weapon) {
		this.weapon = weapon;
	}
}
