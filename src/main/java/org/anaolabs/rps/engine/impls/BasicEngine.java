/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.impls;

import org.anaolabs.rps.engine.IEngine;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.players.Player;
import org.anaolabs.rps.engine.results.GameIssue;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.engine.rules.Action;
import org.anaolabs.rps.engine.rules.Rules;
import org.anaolabs.rps.exceptions.ConfigurationException;
import org.anaolabs.rps.exceptions.UiException;

/**
 * A basic engine for gaming
 *
 * @author Sylvain RICHET
 *
 */
public class BasicEngine implements IEngine {

	/* Rules for this engine */
	protected Rules rules = null;

	/*
	 * (non-Javadoc)
	 * @see org.anaolabs.rps.engine.IEngine#run(org.anaolabs.rps.engine.players.Player, org.anaolabs.rps.engine.players.Player)
	 */
	@Override
	public GameResult run(Player playerA, Player playerB) throws UiException, ConfigurationException {
		
		GameResult gameResult = null;
		
		// Check rules
		if (rules == null || rules.isEmpty()) {
			throw new ConfigurationException("No rule was provided for this engine");
		}
		
		// Check players
		if (playerA == null || playerB == null) {
			throw new ConfigurationException("Players have not been correctly identified");
		}
		
		// PLAYER A
		Weapon weaponA = playerA.getWeapon();
		
		// PLAYER B
		Weapon weaponB =  playerB.getWeapon();

		/* Compute issue of the match based on given rules
		 * 
		 * REMEMBER THAT:
		 * Existence of given pair in rules denotes Player A is the winner, or at least a tie match.
		 */
		if (rules.contains(weaponA, weaponB)) {
			Action action = rules.getAction(weaponA, weaponB);
			
			// No action found denotes a configuration problem at rules level
			if (action == null)
				throw new ConfigurationException("Missing rules in game configuration");
			
			if (action==Action.EQUALS) {
				gameResult = new GameResult(weaponA, weaponB, action, GameIssue.TIE);
			} else {
				gameResult = new GameResult(weaponA, weaponB, action, GameIssue.WIN);
			}
		} else {
			// Inverse players to get winning action 
			Action action = rules.getAction(weaponB, weaponA);
			
			// No action found by reversing players denotes a configuration problem at rules level
			if (action == null)
				throw new ConfigurationException("Missing rules in game configuration");
			
			// Forge game result
			gameResult = new GameResult(weaponA, weaponB, action, GameIssue.LOOSE);
		}
	
		return gameResult;
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.engine.core.IEngineBooter#setRules(org.anaolabs.rps.engine.rules.Rules)
	 */
	@Override
	public void setRules(Rules rules) {
		this.rules = rules;
	}

}
