/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.impls;

import static org.junit.Assert.assertEquals;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.factories.RulesFactory;
import org.anaolabs.rps.engine.impls.BasicEngine;
import org.anaolabs.rps.engine.players.ComputerPlayer;
import org.anaolabs.rps.engine.players.HumanPlayer;
import org.anaolabs.rps.engine.random.WeaponRandom;
import org.anaolabs.rps.engine.random.mock.MockWeaponRandom;
import org.anaolabs.rps.engine.results.GameIssue;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.engine.rules.Rules;
import org.anaolabs.rps.exceptions.ConfigurationException;
import org.anaolabs.rps.exceptions.UiException;
import org.junit.Before;
import org.junit.Test;

/**
 * Test BasicEngine implementation on nominal usecase
 *
 * @author Sylvain RICHET
 *
 */
public class BasicEngineNominalTest {

	BasicEngine engine = null;
	WeaponRandom randomizer = null;
	
	@Before
	public void init() {
		engine = new BasicEngine();
	}
	
	@Test
	public void testRunInTypeRPSAgainstRock() throws UiException, ConfigurationException {
		
		// Provide rules
		Rules rules = RulesFactory.getRules(GameType.RPS);
		engine.setRules(rules);

		/* Inject mock randomizer based on weapon ROCK
		 * So here different rounds will be played against ROCK
		 */
		randomizer = new MockWeaponRandom(Weapon.ROCK);
		
		// Player A is human
		HumanPlayer playerA = new HumanPlayer();
		
		// Player B is a computer
		ComputerPlayer playerB = new ComputerPlayer();
		playerB.setRandom(randomizer);
		
		// Play with ROCK weapon 
		playerA.setWeapon(Weapon.ROCK);
		GameResult resultRockVsRock = engine.run(playerA, playerB);
		assertEquals("Issue should be TIE !",resultRockVsRock.getIssue(),GameIssue.TIE);
		
		// Play with PAPER weapon 
		playerA.setWeapon(Weapon.PAPER);
		GameResult resultPaperVsRock = engine.run(playerA, playerB);
		assertEquals("Issue should be WIN !",GameIssue.WIN,resultPaperVsRock.getIssue());
		
		// Play with SCISSORS weapon 
		playerA.setWeapon(Weapon.SCISSORS);
		GameResult resultScissorVsRock = engine.run(playerA, playerB);
		assertEquals("Issue should be LOOSE !",GameIssue.LOOSE,resultScissorVsRock.getIssue());

	}
	
	
	@Test
	public void testRunInTypeRPSLSAgainstRock() throws UiException, ConfigurationException {
		
		// Provide rules
		Rules rules = RulesFactory.getRules(GameType.RPSLS);
		engine.setRules(rules);

		/* Inject mock randomizer based on weapon ROCK
		 * So here different rounds will be played against ROCK
		 */
		randomizer = new MockWeaponRandom(Weapon.ROCK);
		
		// Player A is human
		HumanPlayer playerA = new HumanPlayer();
		
		// Player B is a computer
		ComputerPlayer playerB = new ComputerPlayer();
		playerB.setRandom(randomizer);

		
		// Play with LIZARD weapon 
		playerA.setWeapon(Weapon.LIZARD);
		GameResult resultLizardVsRock = engine.run(playerA, playerB);
		assertEquals("Issue should be LOOSE !",GameIssue.LOOSE,resultLizardVsRock.getIssue());
		
		// Play with SPOCK weapon
		playerA.setWeapon(Weapon.SPOCK);
		GameResult resultSpockVsRock = engine.run(playerA, playerB);
		assertEquals("Issue should be WIN !",GameIssue.WIN,resultSpockVsRock.getIssue());

	}
}
