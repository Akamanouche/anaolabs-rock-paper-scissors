/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.rules;

import java.util.HashMap;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * A collection of winning rules.
 * <p>
 * Rules MUST BE expressed as a Map where each Entry is :
 * <li>
 * <ul>key: concatenation of [WINNER weapon]+':'+[LOOSER weapon]</ul>
 * <ul>value: Action describing the win</ul>
 * </li>
 * <p>
 * Please note that :
 * <li>
 * <ul>ONLY winning rules are stored</ul>
 * <ul>Winner comes first in the key part</ul>
 * </li>
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class Rules extends HashMap<String, Action> {
	
	/* SERIAL ID */
	private static final long serialVersionUID = -2156485396220322171L;

	/**
	 * Get the winning action for a given couple of weapons
	 * 
	 * @param winner
	 * @param looser
	 * @return
	 */
	public Action getAction(Weapon winner, Weapon looser) {
		String key = getCouple(winner, looser);
		return get(key);
	}
	
	/**
	 * Says if Rules collection contains a given couple of weapons
	 * 
	 * @param winner
	 * @param looser
	 * @return
	 */
	public boolean contains(Weapon winner, Weapon looser) {
		String key = getCouple(winner, looser);
		return containsKey(key);
	}
	
	/**
	 * Forge a key from a given couple of weapons
	 * 
	 * @param winner
	 * @param looser
	 * @return
	 */
	public String getCouple(Weapon winner, Weapon looser) {
		return winner.toString().concat(":").concat(looser.toString());
	}
	
}
