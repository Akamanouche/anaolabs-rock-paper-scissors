/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.artifacts;

/**
 * Game mode for the Player chooses such as :
 * <li>
 *  <ul>Player To Computer</ul>
 *  <ul>Computer To Computer</ul>
 * </li> 
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public enum GameMode {

	/* Player vs Computer mode */
	HUMAN_vs_COMPUTER("Player vs Computer"),
	
	/* Computer vs Computer mode*/
	COMPUTER_vs_COMPUTER("Computer To Computer");
	
	private String modeDescription;
	
	private GameMode(String description) {
		this.modeDescription = description;
	}

	public String getModeDescription() {
		return modeDescription;
	}
	
}
