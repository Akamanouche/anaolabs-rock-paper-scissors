/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.impls;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.factories.RulesFactory;
import org.anaolabs.rps.engine.impls.BasicEngine;
import org.anaolabs.rps.engine.players.ComputerPlayer;
import org.anaolabs.rps.engine.players.Player;
import org.anaolabs.rps.engine.rules.Rules;
import org.anaolabs.rps.exceptions.ConfigurationException;
import org.anaolabs.rps.exceptions.UiException;
import org.junit.Before;
import org.junit.Test;

/**
 * Test BasicEngine implementation at edge behaviors
 *
 * @author Sylvain RICHET
 *
 */
public class BasicEngineEdgeBehaviorsTest {

	BasicEngine engine = null;
	
	@Before
	public void init() {
		engine = new BasicEngine();		
	}
	
	@Test(expected=ConfigurationException.class)
	public void testPlayWithoutAnyRules() throws UiException, ConfigurationException {
		
		// Players
		Player playerA = null;
		Player playerB = null;
		
		engine.run(playerA, playerB);
	}
	
	
	@Test(expected=ConfigurationException.class)
	public void testPlayWithSomeBadPlayer() throws UiException, ConfigurationException {

		// Provide rules
		Rules rules = RulesFactory.getRules(GameType.RPSLS);
		engine.setRules(rules);
		
		// Players
		Player playerA = null;
		Player playerB = new ComputerPlayer();
		
		engine.run(playerA, playerB);
	}

}
