/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Easytrust
 */
package org.anaolabs.rps.engine.rules;

/**
 * An action denotes a interaction issue between a pair of weapons.
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public enum Action {
	
	// Tie
	EQUALS,
	
	// Common winning actions
	CRUSHES,
	COVERS,
	CUTS,
	
	// Extended wining actions
	SMASHES,
	VAPORIZES,
	EATS,
	POISONS,
	DECAPITATES,
	DISPROVES
}
