/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import org.anaolabs.rps.engine.random.WeaponRandom;
import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.random.RpsWeaponRandom;
import org.anaolabs.rps.engine.random.RpslsWeaponRandom;

/**
 * A factory for random concerns
 *
 * @author Sylvain RICHET
 *
 */
public class RandomFactory {

	/**
	 * Gets an weapon randomizer given a game type
	 *
	 * @param type game type
	 * @return a weapon randomizer
	 */
	public static WeaponRandom getWeaponRandomizer(GameType type) {
		WeaponRandom randomizer = null;
		
		switch (type) {
		case RPS:
			randomizer = new RpsWeaponRandom();
			break;
		case RPSLS:
			randomizer = new RpslsWeaponRandom();
			break;

		default:
			break;
		}
		return randomizer;
	}
}
