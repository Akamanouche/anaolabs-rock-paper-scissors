/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine;

import org.anaolabs.rps.engine.players.Player;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.engine.rules.Rules;
import org.anaolabs.rps.exceptions.ConfigurationException;
import org.anaolabs.rps.exceptions.UiException;


/**
 * An engine is responsible for running the game. What we do is :
 * <li>
 *   <ul>setting rules (with some pre-determined rules)</ul>
 *   <ul>running a 2 players match</ul>
 * </li>
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public interface IEngine {
	
	/**
	 * Provides rules for the engine
	 * 
	 * @param rules
	 */
	public void setRules(Rules rules);
	
	/**
	 * Plays a round based on User choice
	 * 
	 * @param round the User choice for this round
	 * @throws ConfigurationException 
	 * @throws UiException 
	 */
	public GameResult run(Player playerA, Player playerB) throws UiException, ConfigurationException;
	
}
