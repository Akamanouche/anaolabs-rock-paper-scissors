/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.results;

import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.rules.Action;

/**
 * <Put some documentation here>
 *
 * @author Sylvain RICHET
 *
 */
public class GameResult {

	/* Player A weapon */
	Weapon weaponA;

	/* Player B weapon */
	Weapon weaponB;

	/* Action specifying game issue from winner point of view */
	Action action;
	
	/* Game issue expressed from Player A to Player B */
	GameIssue issue;

	/**
	 * 
	 * @param weaponA first player
	 * @param weaponB second player
	 * @param issue result 
	 */
	public GameResult(Weapon weaponA, Weapon weaponB, Action action, GameIssue issue) {
		super();
		this.weaponA = weaponA;
		this.weaponB = weaponB;
		this.action = action;
		this.issue = issue;
		
	}

	/**
	 * @return the weapon of player A
	 */
	public Weapon getWeaponA() {
		return weaponA;
	}

	/**
	 * @return the weapon of player B
	 */
	public Weapon getWeaponB() {
		return weaponB;
	}

	/**
	 * @return the issue
	 */
	public GameIssue getIssue() {
		return issue;
	}

	/**
	 * @return the action
	 */
	public Action getAction() {
		return action;
	}

}
