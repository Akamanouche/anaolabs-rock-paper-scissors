/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import org.anaolabs.rps.ui.TerminalUiHandler;
import org.anaolabs.rps.ui.UiHandler;

/**
 * A factory for UI to be used for User interactions.
 *
 * @author Sylvain RICHET
 *
 */
public class UiFactory {

	
	public static UiHandler getUiHandler(/* TODO : add some discriminant criteria */) {
		
		/*
		 * Add some code based on discriminant criteria for UI Handler instanciation
		 */
		return new TerminalUiHandler();
	}
}
