/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import org.anaolabs.rps.engine.IEngine;
import org.anaolabs.rps.engine.artifacts.GameConfig;
import org.anaolabs.rps.engine.artifacts.GameMode;
import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.impls.BasicEngine;
import org.anaolabs.rps.engine.random.WeaponRandom;
import org.anaolabs.rps.engine.rules.Rules;
import org.anaolabs.rps.exceptions.NotYetImplementedException;

/**
 * A factory for GameEngine to be used
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class GameEngineFactory {

	/**
	 * Get a game engine based on the User choices
	 *  
	 * @param gameConfig chosen game configuration
	 * @return an engine for gaming
	 * @throws NotYetImplementedException 
	 */
	public static IEngine getGameEngine(GameConfig gameConfig) throws NotYetImplementedException {
		
		IEngine engine = null;
		engine = new BasicEngine();
		
		// Setting rules according to game type
		GameType gameType = gameConfig.getGameType();
		Rules rules = RulesFactory.getRules(gameType);
		engine.setRules(rules);
		
		return engine;
	}
	
}
