/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.ui;

import java.util.List;
import java.util.Scanner;

import org.anaolabs.rps.engine.artifacts.GameConfig;
import org.anaolabs.rps.engine.artifacts.GameMode;
import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.factories.WeaponFactory;
import org.anaolabs.rps.engine.results.GameIssue;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.exceptions.NotYetImplementedException;
import org.anaolabs.rps.exceptions.ParameterException;

/**
 * A basic UI Handler in a simple terminal    
 *
 * @author Sylvain RICHET
 *
 */
public class TerminalUiHandler implements UiHandler {
	
	/*
	 *  Using scanner for user interactions
	 *  (centralized because of weird behaviors if instantiated more than once in a JVM !...) 
	 *  
	 */
	Scanner scanner = null;

	/* Terminal formatted Strings */

	private static String LS = System.getProperty("line.separator");

	private static final String INTRODUCTION = 
			LS
			.concat("Want to play to ROCK-PAPER-SCISSORS games (basic or extended) ?")
			.concat(LS).concat(LS)
			.concat("\t !! WELCOME YOU !!")
			.concat(LS).concat(LS).concat(LS)
			.concat("(Please provide more informations below...)")
			;
	
	private static final String TYPE_INFO = 
			LS
			.concat("# Which game would you like to play ? Please type :")
			.concat(LS).concat(LS)
			.concat("\t- '1' :\tSimple Rock-Paper-Scissors").concat(LS)
			.concat("\t- '2' :\tSimple Rock-Paper-Scissors-Lizard-Spock").concat(LS)
			;
	
	private static final String MODE_INFO =
			LS
			.concat("# Which mode would you like to use ? Please type : ")
			.concat(LS).concat(LS)
			.concat("\t- '1' :\tPlayer to Computer").concat(LS)
			.concat("\t- '2' :\tComputer to Computer").concat(LS)
			;
	
	private static final String WEAPON_INFO =
			LS
			.concat("# Which weapon do you choose ? Please type : ")
			.concat(LS)
			;
			;
	
	private static final String PLAYERS =
			LS
			.concat("The run was between :")
			.concat(LS)
			.concat("\t - PLAYER A : %s")
			.concat(LS)
			.concat("\t - PLAYER B : %s")
			.concat(LS)
			;
//
//			LS
//			.concat("The run was between :\t PLAYER A is %s || vs || PLAYER B is %s")
//			.concat(LS)
			;
	
	private static final String WEAPONS =
			"Facing weapons are :"
			.concat(LS)
			.concat("\t - PLAYER A played : %s")
			.concat(LS)
			.concat("\t - PLAYER B played : %s")
			.concat(LS)
			;
	
	private static final String RESULTS =
			LS
			.concat("The result is :")
			.concat(LS).concat(LS)
			.concat("\t %s")
			.concat(LS)
			;

	/*
	 * (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#init()
	 */
	@Override
	public void init() {
		
		// Init scanner for user session
	    scanner = new Scanner(System.in);
	    
	    // display some introduction

	    //  Forge GUI interaction
	    System.out.println(String.format("%080d", 0).replace('0','#'));
	    System.out.println(INTRODUCTION);
	    System.out.println(String.format("%080d", 0).replace('0','#'));	    
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#close()
	 */
	@Override
	public void close() {
		// Close scanner
		scanner.close();
	}


	/* (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#displayResult()
	 */
	@Override
	public void displayResult(GameResult result, GameMode mode) {
		
		GameIssue issue = result.getIssue();
		String resultAsString = null;
		
		String playerA = null;
		String playerB = null;
		
		// Provide comments on players according game mode
		switch (mode) {
		case HUMAN_vs_COMPUTER:
			playerA = "YOU";
			playerB = "Computer";
			break;
		case COMPUTER_vs_COMPUTER:
			playerA = "Computer A";
			playerB = "Computer B";
			break;
			
		default:
			playerA = "unknown";
			playerA = "unknown";
			break;
		}
	
		// Format result
		switch (issue) {
		case TIE:
			resultAsString = String.format("TIE MATCH !... with %s on both side", result.getWeaponA());
			break;
			
		case WIN :
			resultAsString = String.format("<%s> WINS !!... with %s which \"%s\" <%s> %s",
					playerA,
					result.getWeaponA(),
					result.getAction(),
					playerB,
					result.getWeaponB());
			break;
			
		case LOOSE :
			resultAsString = String.format("<%s> LOOSES... with <%s> %s which \"%s\" <%s> %s",
					playerA,
					playerB,
					result.getWeaponB(),
					result.getAction(),
					playerA,
					result.getWeaponA());
			break;
		}
		
		// Display
	    System.out.println(String.format("%080d", 0).replace('0','-'));
	    System.out.println(String.format(PLAYERS, playerA, playerB));
	    System.out.println(String.format(WEAPONS, result.getWeaponA(), result.getWeaponB()));
	    System.out.println(String.format(RESULTS, resultAsString));
	    System.out.println(String.format("%080d", 0).replace('0','-'));
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#displayError(java.lang.Exception)
	 */
	@Override
	public void displayError(Exception ex) {
	    System.out.println(String.format("%080d", 0).replace('0','-'));
	    System.out.println(String.format("!! ERROR !! %s \t==> %s %s", LS, ex.getMessage(), LS));
	    System.out.println("Please try again");
	    System.out.println(String.format("%080d", 0).replace('0','-'));	    
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#getUserGameConfig()
	 */
	@Override
	public GameConfig getUserGameConfig() throws ParameterException, NotYetImplementedException {
		
		GameConfig userConfig = null;
	    
	    // Display and catch GAME information
	    System.out.println(TYPE_INFO);
 
	    GameType game = null;
	    try {
	    	// gets game from scanner
	    	System.out.print("Choice: ");
	    	int gameInfo = scanner.nextInt();
		    game = GameType.values()[gameInfo-1];
		} catch (Exception e) {
			scanner.close();
			throw new ParameterException("Provided GAME number is incorrect !");
		}

	    // Display and catch MODE information
	    System.out.println(MODE_INFO);
	    GameMode mode = null;
	    try {
	    	// gets mode from scanner
	    	System.out.print("Choice: ");
		    int modeInfo = scanner.nextInt();
		    mode = GameMode.values()[modeInfo-1];
		} catch (Exception e) {
			scanner.close();
			throw new ParameterException("Provided MODE number is incorrect !");
		}
	    
	    // Forge game configuration as chosen by User
	    userConfig = new GameConfig(game, mode);

		return userConfig;
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.ui.UiHandler#getUserRound()
	 */
	@Override
	public Weapon getWeapon(GameConfig config) throws ParameterException, NotYetImplementedException {
		
	    Weapon weapon = null;

	    // Ask User for a weapon only for "PLAYER TO COMPUTER" mode
	    GameMode mode = config.getGameMode();
	    GameType type = config.getGameType();
	    if (mode == GameMode.HUMAN_vs_COMPUTER) {
		    List<Weapon> availableWeapons = WeaponFactory.getWeapons(type);
		    System.out.println(WEAPON_INFO);
		    for (Weapon avElt : availableWeapons) {
			    System.out.println(String.format("\t- '%d' :\t%s", avElt.ordinal()+1,avElt.toString()));
			}
		    
		    try {
		    	// get weapon from scanner
		    	System.out.print("Choice: ");
		    	int weaponInfo = scanner.nextInt();
		    	weapon = availableWeapons.get(weaponInfo-1);

			} catch (Exception e) {
				throw new ParameterException("Provided WEAPON number is incorrect !");
			}
	    }
	    
		return weapon;
	}
	
	
}
