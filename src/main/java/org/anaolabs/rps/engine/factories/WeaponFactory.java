/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import java.util.ArrayList;
import java.util.List;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * Factory for available weapons extraction
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class WeaponFactory {

	
	/**
	 * Get list of available weapons for a given game type
	 *   
	 * @param type game type
	 * @return list of available weapons
	 */
	public static List<Weapon> getWeapons(GameType type) {
		
		List<Weapon> weapons = new ArrayList<Weapon>();
		
		switch (type) {
		case RPS:
			for (int i = 0; i < Weapon.values().length-2; i++) {
				weapons.add(Weapon.values()[i]);
			}
			break;
		case RPSLS:
			for (int i = 0; i < Weapon.values().length; i++) {
				weapons.add(Weapon.values()[i]);
			}
			break;

		default:
			break;
		}
		
		return weapons;
	}
	
}
