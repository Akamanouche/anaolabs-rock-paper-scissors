/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps;

import org.anaolabs.rps.engine.IEngine;
import org.anaolabs.rps.engine.artifacts.GameConfig;
import org.anaolabs.rps.engine.artifacts.GameMode;
import org.anaolabs.rps.engine.factories.GameEngineFactory;
import org.anaolabs.rps.engine.factories.RandomFactory;
import org.anaolabs.rps.engine.factories.UiFactory;
import org.anaolabs.rps.engine.players.ComputerPlayer;
import org.anaolabs.rps.engine.players.HumanPlayer;
import org.anaolabs.rps.engine.players.Player;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.ui.UiHandler;

/**
 * This is the main engine runner responsible for :
 * <li>
 *  <ul>orchestrating interaction between player and engine with UI</ul>
 *  <ul>providing player to choose game mode and type</ul>
 * </li> 
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class RpsEngineRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		/* Engine running the game */
		IEngine engine = null;
		
		/* UI used for User interactions */
		UiHandler ui = null;
		
		/* Game configuration */
		GameConfig gameConfig = null;
		
		/* Game result */
		GameResult result = null;

		try {
			// Get UI and init
			ui = UiFactory.getUiHandler();
			ui.init();

			// Get game configuration from UI 
			gameConfig = ui.getUserGameConfig();
			
		    // Get game engine configuration from User 
		    engine = GameEngineFactory.getGameEngine(gameConfig);

		    // 2 players
		    Player playerA = null;
		    Player playerB = null;
		    
		    // Player A can be either a human or a computer (depending on game mode)
		    if (gameConfig.getGameMode() == GameMode.HUMAN_vs_COMPUTER) {
		    	playerA = new HumanPlayer();
		    	((HumanPlayer)playerA).setWeapon(ui.getWeapon(gameConfig));
		    } else {
			    playerA = new ComputerPlayer();
			    ((ComputerPlayer)playerA).setRandom(RandomFactory.getWeaponRandomizer(gameConfig.getGameType()));
		    }
		    
		    // Player B is always a computer
		    playerB = new ComputerPlayer();
		    ((ComputerPlayer)playerB).setRandom(RandomFactory.getWeaponRandomizer(gameConfig.getGameType()));
		    
		    // Play with given Round
		    result = engine.run(playerA, playerB);

		    // Display result with UI
		    ui.displayResult(result,gameConfig.getGameMode());
			
		} catch (Exception ex) {
			// UI displays exception
			ui.displayError(ex);
			
		} finally {
		    // Close UI
		    ui.close();
		}
	}
}
