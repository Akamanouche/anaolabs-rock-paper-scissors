/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.random;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * Denotes an randomizer of {@link Weapon}
 *
 * @author Sylvain RICHET
 *
 */
public interface WeaponRandom {

	/**
	 * Get next weapon based on random
	 *
	 * @return a weapon
	 */
	public Weapon nextWeapon();
}
