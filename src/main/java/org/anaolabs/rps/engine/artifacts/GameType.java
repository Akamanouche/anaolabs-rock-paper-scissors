/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.artifacts;

/**
 * Game type the player wants to play such as :
 * <li>
 *   <ul>simple Rock-Paper-Scissors</ul>
 *   <ul>extended Rock-Paper-Scissors</ul>
 * </li>
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public enum GameType {

	/* Simple game "Rock-Paper-Scissors" */
	RPS("Rock-Paper-Scissors"),
	
	/* Extended game "Rock-Paper-Scissors-Lizard-Spock" */
	RPSLS("Rock-Paper-Scissors-Lizard-Spock");
	
	private String gameDescription;
	
	private GameType(String description) {
		this.gameDescription = description;
	}

	public String getGameDescription() {
		return gameDescription;
	}
	
	
}
