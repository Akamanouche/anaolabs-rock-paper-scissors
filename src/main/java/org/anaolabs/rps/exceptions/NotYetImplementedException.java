/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.exceptions;

/**
 * Handles exception concerning not yet implemented feature
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class NotYetImplementedException extends Exception {

	/* SERIAL ID */
	private static final long serialVersionUID = -7982699724722032562L;

	/**
	 * 
	 */
	public NotYetImplementedException() {
	}

	/**
	 * @param message
	 */
	public NotYetImplementedException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public NotYetImplementedException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotYetImplementedException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NotYetImplementedException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
