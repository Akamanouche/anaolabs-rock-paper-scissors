/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.exceptions;

/**
 * Denotes an exception thrown at engine configuration level
 *
 * @author Sylvain RICHET
 *
 */
public class ConfigurationException extends Exception {

	/* SERIAL ID */
	private static final long serialVersionUID = 4159876296500009057L;

	/**
	 * 
	 */
	public ConfigurationException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ConfigurationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ConfigurationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ConfigurationException(Throwable cause) {
		super(cause);
	}

}
