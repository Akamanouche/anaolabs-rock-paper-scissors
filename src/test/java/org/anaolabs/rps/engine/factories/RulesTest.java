/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.rules.Action;
import org.anaolabs.rps.engine.rules.Rules;
import org.junit.Test;

/**
 * Tests around Rules
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class RulesTest {

	@Test
	public void testGetRulesForRPS() {
		GameType game = GameType.RPS;
		Rules rules = RulesFactory.getRules(game);
		
		// Check/Assertions
		assertEquals("Size SHOULD BE of: 6 winning couples.", 6, rules.size());
		assertTrue(rules.contains(Weapon.ROCK, Weapon.SCISSORS));
		assertEquals(Action.CRUSHES, rules.getAction(Weapon.ROCK, Weapon.SCISSORS));

		assertTrue(rules.contains(Weapon.ROCK, Weapon.ROCK));
		assertEquals(Action.EQUALS, rules.getAction(Weapon.ROCK, Weapon.ROCK));
	}

}
