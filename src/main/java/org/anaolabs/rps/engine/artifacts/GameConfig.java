/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.artifacts;

/**
 * Brings the game configuration as chosen by User, it means :
 * <li>
 * <ul>a game mode</ul>
 * <ul>a game type</ul>
 * </li>
 *
 * @author Sylvain RICHET
 *
 */
public class GameConfig {

	GameMode mode;
	GameType game;
	
	/**
	 * 
	 */
	public GameConfig(GameType game, GameMode mode) {
		this.game = game;
		this.mode = mode;
	}

	public GameMode getGameMode() {
		return mode;
	}

	public GameType getGameType() {
		return game;
	}

}
