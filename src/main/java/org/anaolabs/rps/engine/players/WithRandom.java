/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.players;

import org.anaolabs.rps.engine.random.WeaponRandom;

/**
 * Denotes the need of having a weapon randomizer
 *
 * @author Sylvain RICHET
 *
 */
public interface WithRandom {

	/**
	 * Sets an inner randomizer for weapons 
	 */
	public void setRandom(WeaponRandom randomizer);

}
