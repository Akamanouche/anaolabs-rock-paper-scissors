/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.exceptions;

/**
 *  Denotes an exception thrown at UI level
 *
 * @author Sylvain RICHET
 *
 */
public class UiException extends Exception {

	/* SERIAL ID */
	private static final long serialVersionUID = -4259604529570548838L;

	/**
	 * 
	 */
	public UiException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UiException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public UiException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public UiException(Throwable cause) {
		super(cause);
	}
}
