/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.factories.WeaponFactory;
import org.junit.Test;

/**
 * Test on WeaponFactory class
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class WeaponFactoryTest {
	
	@Test
	public void testGetWeaponsforRPS() {
		GameType game = GameType.RPS;
		List<Weapon> weapons = WeaponFactory.getWeapons(game);
		
		// Check/Assertions
		assertEquals("Size SHOULD BE of: 3 weapons.", 3, weapons.size());
		assertTrue(weapons.contains(Weapon.ROCK));
		assertFalse(weapons.contains(Weapon.LIZARD));
		assertFalse(weapons.contains(Weapon.SPOCK));
	}
	
	@Test
	public void testGetWeaponsforRPSLS() {
		GameType game = GameType.RPSLS;
		List<Weapon> weapons = WeaponFactory.getWeapons(game);
		
		// Check/Assertions
		assertEquals("Size SHOULD BE of: 5 weapons.", 5, weapons.size());
		assertTrue(weapons.contains(Weapon.ROCK));
		assertTrue(weapons.contains(Weapon.LIZARD));
		assertTrue(weapons.contains(Weapon.SPOCK));
	}

}
