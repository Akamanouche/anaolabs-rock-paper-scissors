/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.players;

import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.random.WeaponRandom;

/**
 * A Computer player choosing its weapon with a randomizer
 *
 * @author Sylvain RICHET
 *
 */
public class ComputerPlayer implements Player, WithRandom {

	/* Weapon chosen by randomizer */ 
	Weapon weapon = null;
	
	/* Weapon randomizer */
	WeaponRandom randomizer;

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.engine.players.Player#getWeapon()
	 */
	@Override
	public Weapon getWeapon() {
		return randomizer.nextWeapon();
	}

	/* (non-Javadoc)
	 * @see org.anaolabs.rps.engine.players.WithRandom#setRandom(org.anaolabs.rps.engine.random.WeaponRandom)
	 */
	@Override
	public void setRandom(WeaponRandom randomizer) {
		this.randomizer = randomizer;
	}

}
