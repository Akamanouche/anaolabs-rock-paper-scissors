Rock-Paper-Scissors game : a basic implementation
================

Here is a basic sample of so-called **Rock-Paper-Scissors** game, as described also [here](https://en.wikipedia.org/wiki/Rock-paper-scissors).  

This implementation relies on **Java** language without any external libraries (except *JUnit* for testing).

The UI is very basic : a command line UI with a minimal interaction.


### Building

Project is build with Maven. In a Linux terminal, simply launch below :
```
$ cd /path/to/your/github/anaolabs-rock-paper-scissors
$ mvn clean install
```

### Playing

In a Linux terminal, you should at least build the project (as mentionned above), and then simply launch below :
```
$ java -jar target/anaolabs-rock-paper-scissors-2.0.0.jar
(or)
$ java -jar /path/to/your/local/maven/repository/org/anaolabs/rps/anaolabs-rock-paper-scissors/2.0.0/anaolabs-rock-paper-scissors-2.0.0.jar
```
Just follow the invitations at prompt.


### Extending

  - Extending to game with Lizard and Spock artifacts (as mentionned [here](https://en.wikipedia.org/wiki/Rock-paper-scissors#Additional_weapons)) operates.

  - *Computer to Computer* mode works fine as well.
