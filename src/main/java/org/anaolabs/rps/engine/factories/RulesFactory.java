/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.factories;

import org.anaolabs.rps.engine.artifacts.GameType;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.rules.Action;
import org.anaolabs.rps.engine.rules.Rules;

/**
 * Factory for rules to follow  
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class RulesFactory {

	/**
	 * Provides the {@link Rules} for a given game 
	 * <p>
	 * PLEASE NOTE THAT :
	 * <li>
	 *  <ul>Rules are provided from the point of view of Player A</ul>
	 *  <ul>Only tie and winner rules are mentionned</ul>
	 *  <ul>So if no rule appears concerning a pair, it means Player A loosed the match</ul>
	 * </li> 
 	 * 
	 * @param type
	 * @return 
	 */
	public static Rules getRules(GameType type) {
		Rules rules = new Rules();
		
		switch (type) {

		case RPSLS:
			// Extended weapons Winning rules
			rules.put(rules.getCouple(Weapon.SPOCK,Weapon.SCISSORS), Action.SMASHES);
			rules.put(rules.getCouple(Weapon.SPOCK,Weapon.ROCK), Action.VAPORIZES);
			rules.put(rules.getCouple(Weapon.LIZARD,Weapon.SPOCK), Action.POISONS);
			rules.put(rules.getCouple(Weapon.LIZARD,Weapon.PAPER), Action.EATS);
			
			rules.put(rules.getCouple(Weapon.SCISSORS,Weapon.LIZARD), Action.DECAPITATES);
			rules.put(rules.getCouple(Weapon.PAPER,Weapon.SPOCK), Action.DISPROVES);
			rules.put(rules.getCouple(Weapon.ROCK,Weapon.LIZARD), Action.CRUSHES);
			
			// Extended weapons Tie rules
			rules.put(rules.getCouple(Weapon.SPOCK,Weapon.SPOCK), Action.EQUALS);
			rules.put(rules.getCouple(Weapon.LIZARD,Weapon.LIZARD), Action.EQUALS);

			// NO BREAK here to keep on "commons" rules below

		case RPS:
			// Common weapons Winning rules
			rules.put(rules.getCouple(Weapon.ROCK,Weapon.SCISSORS), Action.CRUSHES);
			rules.put(rules.getCouple(Weapon.PAPER,Weapon.ROCK), Action.COVERS);
			rules.put(rules.getCouple(Weapon.SCISSORS,Weapon.PAPER), Action.CUTS);
	
			// Common weapons Tie rules
			rules.put(rules.getCouple(Weapon.ROCK,Weapon.ROCK), Action.EQUALS);
			rules.put(rules.getCouple(Weapon.PAPER,Weapon.PAPER), Action.EQUALS);
			rules.put(rules.getCouple(Weapon.SCISSORS,Weapon.SCISSORS), Action.EQUALS);			
			
			break;
		}
		return rules;
	}
}
