/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.random;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * An weapon randomizer for RPS game type
 *
 * @author Sylvain RICHET
 *
 */
public class RpsWeaponRandom implements WeaponRandom {

	/*
	 * (non-Javadoc)
	 * @see org.anaolabs.rps.engine.random.WeaponRandom#nextWeapon()
	 */
	@Override
	public Weapon nextWeapon() {
		Weapon weapon = null;
		Double play = Math.random();

		if (play < 0.33) {
			weapon = Weapon.ROCK;
		} else if (play < 0.67) {
			weapon = Weapon.PAPER;
		} else {
			weapon = Weapon.SCISSORS;
		}
		
		return weapon;
	}

}
