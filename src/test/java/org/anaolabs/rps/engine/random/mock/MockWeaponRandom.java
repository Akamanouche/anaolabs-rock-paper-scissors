/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.random.mock;

import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.random.WeaponRandom;

/**
 * A mock weapon randomizer which always get the same weapon (so called "seed" here)
 *
 * @author Sylvain RICHET
 *
 */
public class MockWeaponRandom implements WeaponRandom {

	// Static weapon
	Weapon seed = null;
	
	/**
	 * @param seed constant weapon to provide
	 */
	public MockWeaponRandom(Weapon seed) {
		super();
		this.seed = seed;
	}


	/* (non-Javadoc)
	 * @see org.anaolabs.rps.engine.random.WeaponRandom#nextWeapon()
	 */
	@Override
	public Weapon nextWeapon() {
		return seed;
	}

}
