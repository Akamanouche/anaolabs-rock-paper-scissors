/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.ui;

import java.util.Optional;

import org.anaolabs.rps.engine.artifacts.GameConfig;
import org.anaolabs.rps.engine.artifacts.GameMode;
import org.anaolabs.rps.engine.artifacts.Weapon;
import org.anaolabs.rps.engine.results.GameResult;
import org.anaolabs.rps.exceptions.NotYetImplementedException;
import org.anaolabs.rps.exceptions.ParameterException;

/**
 * A UI Handler is responsible for managing interactions with User.
 *
 * @author Sylvain RICHET
 *
 */
public interface UiHandler {
	
	/**
	 * Init UI
	 */
	public void init();

	/**
	 * Closes UI
	 */
	public void close();
	
	/**
	 * Gets user choices concerning game type and mode
	 *
	 * @return a configuration game based on User choices
	 * @throws ParameterException 
	 * @throws NotYetImplementedException 
	 */
	public GameConfig getUserGameConfig() throws ParameterException, NotYetImplementedException;


	/**
	 * Gets weapon user choice
	 *
	 * @param game config as previously chosen by User
	 * @return a weapon
	 * @throws ParameterException 
	 * @throws NotYetImplementedException 
	 */
	public Weapon getWeapon(GameConfig config) throws ParameterException, NotYetImplementedException;

	/**
	 * Displays results of current round
	 * 
	 * @param result result to display
	 */
	public void displayResult(GameResult result, GameMode mode);
	
	
	/**
	 * Displays Errors
	 */
	public void displayError(Exception ex);
	
}
