/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.players;

import org.anaolabs.rps.engine.artifacts.Weapon;

/**
 * A player running the game.
 *
 * @author Sylvain RICHET
 *
 */
public interface Player {

	/**
	 * Get weapon as chosen by a human or a weapon randomizer  
	 *
	 * @return
	 */
	public Weapon getWeapon();
}
