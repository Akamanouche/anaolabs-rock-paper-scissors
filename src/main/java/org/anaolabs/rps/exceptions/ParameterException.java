/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.exceptions;

/**
 * Handles parameter exception on parameters/choices as provided by User
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public class ParameterException extends Exception {

	/* SERIAL ID */
	private static final long serialVersionUID = 2620145751279512411L;
	
	/**
	 * 
	 */
	public ParameterException() {
	}

	/**
	 * @param message
	 */
	public ParameterException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ParameterException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ParameterException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public ParameterException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
