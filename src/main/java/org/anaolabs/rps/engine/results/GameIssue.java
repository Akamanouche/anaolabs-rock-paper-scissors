/**
 * anaolabs-rock-paper-scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.results;

/**
 * Game issue as expressed from Player A against Player B
 *
 * @author Sylvain RICHET
 *
 */
public enum GameIssue {
	
	/* No winner, no looser */		
	TIE,
	
	/* Player A is the winner */
	WIN,
	
	/* Player B is the winner */
	LOOSE
}
