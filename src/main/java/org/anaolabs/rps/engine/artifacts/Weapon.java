/**
 * anaolabs-Rock-Paper-Scissors
 *
 * Copyright (c) Anaolabs
 */
package org.anaolabs.rps.engine.artifacts;

/**
 * An weapon as chosen by User for current round.
 *
 * Aug 11, 2016
 * @author SylvainRichet
 *
 */
public enum Weapon {

	/* Common weapons */
	ROCK,
	PAPER,
	SCISSORS,
	
	/* below: extended weapons */
	LIZARD,
	SPOCK;
}
